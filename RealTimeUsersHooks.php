<?php

/**
 * All hooked functions used by RealTimeUsers
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 */

class RealTimeUsersHooks {

	/**
	 * Set up the parser hooks
	 * @param object $parser: instance of OutputPage
	 * @return Boolean: true
	 */
	public static function registerParserHook( &$parser ) {
		$parser->setHook( 'realtimeusers', 'RealTimeUsersHooks::realTimeUsersRender' );
		return true;
	}

	/**
	 * Callback function for registerParserHook
	 * @param string $input: user-supplied input, unused
	 * @param array $args: user-supplied arguments, unused
	 * @param object $parser: instance of Parser, unused
	 * @return String: HTML
	 */
	public static function realTimeUsersRender( $input, $args, $parser ) {

		$context = new RequestContext();
		$out = $context->getOutput();
		$config = $context->getConfig();

		$refreshInterval = $config->get("refreshInterval");
		$output = "<div id='rtContainer'>";

		$mode = 'combo';
		if(in_array($args['mode'], ['number','chart'])) $mode = $args['mode'];

		// prepare data
		$data = RealTimeUsersHooks::getChartData('today');
		$points1 = [];
		foreach($data as $point) {
			array_push($points1, "{\"x\":\"$point[1]\",\"y\":$point[0]}");
		}
		$data = RealTimeUsersHooks::getChartData('yesterday');
		$points2 = [];
		foreach($data as $point) {
			array_push($points2, "{\"x\":\"$point[1]\",\"y\":$point[0]}");
		}

		if(in_array($mode, ['number', 'combo'])) {
			// refreshing number of RT users
			$output .= "<div id='rtUsers'>" . $out->msg( 'realtimeusers-boxtext' )->text() . "</div>";
		}

		if(in_array($mode, ['chart', 'combo'])) {
			// chart of active users
			$output .= "<canvas id='rtuChart' width='400' height='300' data-refresh='$refreshInterval' data-chart1='[" . implode(',', $points1) . "]' data-chart2='[" . implode(',', $points2) . "]'></canvas></div>";
		}
						
		$output .= "</div>";
		$out->addModules('ext.RealTimeUsers');
		return $output;
	}

	/**
	 * Show chart
	 * @param object $out: instance of OutputPage
	 * @param object $skin: instance of Skin, unused
	 */
	public static function showChart( &$out, &$skin ) {

		$title = $out->getTitle();
		if($title->getArticleID() == 1 && !isset( $query['action'] )) {
			// mainpage only
			$out->addModules('ext.RealTimeUsers');
		}
		return true;
	}


	/**
	 * Get data for RT users chart
	 * @param string $interval: 'today|yesterday'
	 * @return array: [number of RT users,datetime]
	 */
	public static function getChartData($interval) {

		$now = new DateTime();
		switch($interval) {
			case 'today':
			$datestr = $now->format('Y-m-d');
			break;

			case 'yesterday':
			$now->modify("-1 day");
			$datestr = $now->format('Y-m-d');
			break;

			default:
			return false;
		}

		$output = array();
		$handle = @fopen(__DIR__ . "/data/chart.csv", "r");
		while (($buffer = fgets($handle, 4096)) !== false) {
			preg_match("/^([0-9]*);(.*)$/", rtrim($buffer), $m);
			$number = $m[1];
			$date = $m[2];
			if(preg_match("/$datestr/", $date)) {
				$date = preg_replace("/[0-9]{4}-[0-9]{2}-[0-9]{2} /", '', $date);
				array_push($output, array($number, $date));
			}
		}
		fclose($handle);
		return $output;
	}
}