# RealTimeUsers

Mediawiki extension.

## Description

* Extension gathers and displays info about wiki's real time users. Information is taken from server access log.
* Active users are those who have sent a hit within the last five minutes.
* Adding tag <realtimeusers mode=""></realtimeusers>
    * mode="number": numer of RT users refreshing in given interval (default 30s)
    * mode="chart": chart of RT users' snapshots in given intervals (default 30min)
    * mode="combo": box with complete available information (default choice)
* It works on main page only

## Installation

* Make sure you have MediaWiki 1.29+ installed.
* Download and place the extension to your _/extensions/_ folder.
* Set variables in _extension.json_.
* Create symlinks to logfiles (with correct rights)
    * `ln -s /var/log/..../????.access.log data/wiki.log`.
    * `ln -s /var/log/..../????.access.log.0.bz2 data/wiki.log.0.bz2`.
    * `ln -s /var/log/..../????.access.log.1.bz2 data/wiki.log.1.bz2`.
    * `ln -s /var/log/..../????.access.log.2.bz2 data/wiki.log.2.bz2`.
    * `ln -s /var/log/..../????.access.log.3.bz2 data/wiki.log.3.bz2`.
    * `ln -s /var/log/..../????.access.log.4.bz2 data/wiki.log.4.bz2`.
* Set Cron job _cron/saveRecentRealTimeUsers.py_ at every 30th minute `*/30 * * * *`.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'RealTimeUsers' )`;
* _cron/saveRecentRealTimeUsers.py_ shouldn't be accessible from web.

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2020 First Faculty of Medicine, Charles University
