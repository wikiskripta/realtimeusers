/**
 * Javascript for extension
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

( function ( mw, $ ) {

	var wikipath = window.location.origin;

	/***************************/
	/*      Display chart      */
	/***************************/
	
	var ctx = $('#rtuChart');
	moment.locale('cs');

	var data1 = ctx.data('chart1');
	var data2 = ctx.data('chart2');
	
	var timeFormat = 'hh:mm';

    var config = {
        type:    'line',
        data:    {
            datasets: [
                {
                    label: mw.message("realtimeusers-today").text(),
                    data: data1,
                    fill: true,
                    borderColor: '#96bbcc'
                },
                {
                    label: mw.message("realtimeusers-yesterday").text(),
                    data: data2,
                    fill: true,
                    borderColor: '#dcdcdc'
                }
            ]
        },
        options: {
            responsive: false,
            title:      {
                display: false,
                text:    mw.message("realtimeusers-title").text()
            },
            scales: {
                xAxes: [{
                    type:       "time",
                    time: 		{
						parser: timeFormat,
						unit: 'hour',
						unitStepSize: 1,
						tooltipFormat: 'lll',
						displayFormats: {
							'hour': 'H',
						}
					},
                    scaleLabel: {
                        display:     true,
                        labelString: mw.message("realtimeusers-xAxis").text()
                    }
                }],
                yAxes: [{
                    ticks: {
                        precision:0
                    },
                    scaleLabel: {
                        display:     true,
                        labelString: mw.message("realtimeusers-yAxis").text()
                    }
                }]
            }
        }
    };

	var chart = new Chart(ctx, config);


	/***************************/
	/* Update RT users number  */
	/***************************/

    var refreshInterval = 1000 * ctx.data("refresh");
    $('#rtUsers strong').load(wikipath + '/index.php?title=Special:RealTimeUsers/getNumber', function() {
        $('#rtUsers').fadeIn(1200);
        setInterval(function(){
            if ($('#rtUsers').is(":visible")) {
                $.get( wikipath + '/index.php?title=Special:RealTimeUsers/getNumber', function(aunmbr) {
                    $('#rtUsers').fadeOut(600);
                    $('#rtUsers strong').text(aunmbr);
                    $('#rtUsers').fadeIn(1200);               
                })
            }
        }, refreshInterval );
    });
    

}( mediaWiki, jQuery ) );
