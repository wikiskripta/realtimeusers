# Save recent number of realtime users to "data/chart.csv"
# Call https://www.wikiskripta.eu/w/Special:RealTimeUsers/getNumber and save result to "data/chart.csv"
# Run in cron, choose interval from [5,10,15,20,30,60]
# @ingroup Extensions
# @author Josef Martiňák
# @license MIT

import requests
import os
from datetime import datetime

dir_path = os.path.dirname(os.path.realpath(__file__))
response = requests.get("https://www.wikiskripta.eu/index.php?title=Special:RealTimeUsers/getNumber", timeout = 15)
rtNumber = response.content.decode("utf-8")

# get current datetime
now = datetime.now()
dt_string = now.strftime("%Y-%m-%d %H:%M")

# save value to file, allow max lines 192 (4 days when checking once a 30 min)
with open(dir_path + '/../data/chart.csv', 'r') as f:
    chLines = f.readlines()
if len(chLines)>191:
    with open(dir_path + '/../data/chart.csv', 'w') as fin:
        for i in range(192-len(chLines),1+len(chLines)):
            f.write(chLines[i])
        f.write(rtNumber + ';' + dt_string + '\n') # recent value
else:
    with open(dir_path + '/../data/chart.csv', 'a') as f:
        f.write(rtNumber + ';' + dt_string + '\n') # recent value