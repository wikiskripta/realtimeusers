<?php

/**
 * SpecialPage for RealTimeUsers extension
 * Show info and realtime users' number
 * Called with "/getNumber" param returns the number of RT users
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 */

class RealTimeUsers extends SpecialPage {
	
	function __construct() {
		parent::__construct( 'RealTimeUsers' );
	}

	function execute($param) {
		
		$this->setHeaders();
		$out = $this->getOutput();
		$config = $out->getConfig();

		if($param == 'getNumber') {
			// Output raw number of RT users
			$out->disable();
			header( 'Content-type: application/text; charset=utf-8' );
			$rtusers = [];
			$done = false;
			$started = false;

			try {
				$handle = @fopen(__DIR__ . "/data/wiki.log", "r");
   				while (($buffer = fgets($handle, 4096)) !== false) {
					if(preg_match("/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[^\[]*\[([^ ]*)/", $buffer, $m)) {
						//188.75.128.28 - - [02/Jan/2020:00:01:02 +0100]
						$date = DateTime::createFromFormat('d/M/Y:H:i:s', $m[2]);
						$now = new DateTime();
						//$now = new DateTime("2020-01-07 14:00:00"); // pro testování
						$diff = $now->getTimestamp() - $date->getTimestamp();
						if($diff<300) {
							// found hits from last 5 minutes
							array_push($rtusers, $m[1]);
							$started = true;
						}
						elseif($started) {
							$done = true;
							break;
						}
					}
				}
				fclose($handle);
				if(!$started) $done = true; // no recent hit in log, stop searching

				// check older logs if necessary
				if(!$done) {
					for($i=0;$i<5;$i++) {
						$started = false;
						if($bz = @bzopen(__DIR__ . "/data/wiki.log." . (string)$i . ".bz2", "r")) {
							while (!feof($bz)) {
								$buffer = bzread($bz);
								if(preg_match("/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[^\[]*\[([^ ]*)/", $buffer, $m)) {
									$date = DateTime::createFromFormat('d/M/Y:H:i:s', $m[2]);
									$now = new DateTime();
									//$now = new DateTime("2020-01-07 14:00:00"); // pro testování
									$diff = $now->getTimestamp() - $date->getTimestamp();
									if($diff<300) {
										// found hits from last 5 minutes
										array_push($rtusers, $m[1]);
										$started = true;
									}
									elseif($started) {
										$done = true;
										break;
									}
								}
							}
							bzclose($bz);
							if(!$started) $done = true; // no recent hit in log, stop searching
						}
						if($done) break;
					}
				}
			} catch(Exception $e) {
				echo 'err';
			}

			$unique = array_unique($rtusers);
			echo sizeof($unique);
			exit;
		}

		// Display info
		$out->addHTML( "<p>" . $this->msg( 'realtimeusers-desc' )->text() . "</p>" );
		$out->addHTML( "<p>" . $this->msg( 'realtimeusers-manual' )->text() . "</p>" );

		// prepare data
		$data = RealTimeUsersHooks::getChartData('today');
		$points1 = [];
		foreach($data as $point) {
			$point[1] = preg_replace("/[0-9]{4}-[0-9]{2}-[0-9]{2} /", '', $point[1]);
			array_push($points1, "{\"x\":\"$point[1]\",\"y\":$point[0]}");
		}
		$data = RealTimeUsersHooks::getChartData('yesterday');
		$points2 = [];
		foreach($data as $point) {
			$point[1] = preg_replace("/[0-9]{4}-[0-9]{2}-[0-9]{2} /", '', $point[1]);
			array_push($points2, "{\"x\":\"$point[1]\",\"y\":$point[0]}");
		}

		$out->addHTML("<div id='rtContainer'><div id='rtUsers'>" . $this->msg( 'realtimeusers-boxtext' )->text() . "</div>");
		$out->addHTML("<canvas id='rtuChart' width='400' height='300' data-refresh='" . $config->get("refreshInterval") . "' data-chart1='[" . implode(',', $points1) . "]' data-chart2='[" . implode(',', $points2) . "]'></canvas></div>");
		$out->addModules('ext.RealTimeUsers');
	}
}